﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klasside_ülesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Autod.txt";
            string[] failisisu = File.ReadAllLines(failinimi); //loeb ridade kaupa
            foreach(var failirida in failisisu)
            {
                var reaosad = (failirida + ",,,").Split(',');//Neid komasid on selleks vaja, et kindlalt saaks neli osa
                //if (reaosad.Length == 4) Sellega kindlustab selle et tühje ridu ei lisata
                new Auto(reaosad[3].Trim())//TRIM korjab ära tühikud koma ees ja koma taga
                {
                    Tootja = reaosad[0].Trim(),
                    Mudel = reaosad[1].Trim(),
                    SilindriteArv = int.Parse(reaosad[2].Trim())
                };

                foreach (var x in Auto.Autod)
                {
                    Console.WriteLine(x);
                }
                Console.WriteLine(Auto.AutodeTuastaInf["123ABC"]); //trükib välja auto numbrimärgi järgi
                foreach (var x in Auto.AutodeTuastaInf) // trükib välja terve dictionary sisu
                {
                    Console.WriteLine(x);
                }

            }
        }
                }
    }
    class Auto
    {
        public static Dictionary<string, Auto> AutodeTuastaInf = new Dictionary<string, Auto>();
        public static List<Auto> Autod = new List<Auto>();
        public string Tootja;
        public string Mudel;
        public int SilindriteArv = 4;
        public readonly string NumbriM2rk;
        static int autosid = 0; //private, auto järjekorranumber
        int number = ++autosid;


        public Auto(string numbriM2rk) //kõikidel autodel peab olema numbrimärk
        {
            this.NumbriM2rk = numbriM2rk;// Numbrimärk on väli ja numbrivärk on parameeter. Kui sul on need sõnad erinevalt kirjutatud, siis saad this eest ära jätta, sest ta saab ise aru mis on mis. ''This'' on kirjutatud helelillana, mitte tumesinisena, järelikult optional.
            Autod.Add(this);

            if (!AutodeTuastaInf.ContainsKey(numbriM2rk)) AutodeTuastaInf.Add(numbriM2rk, this);//Esimene lause ütleb kui EI OLE seda numbrit, teine lause, ALLES SIIS lisame dictionarysse. Sama numbrimärgi vältimine, kuna dictionarys ei tohi olla kaks korda sama võtmega asja- saad vea
        }

        public Auto() : this("123ABC")
        { }
        public override string ToString() => $"{number}. {Tootja} {Mudel} (numbrimärk on {NumbriM2rk}) ja tal on {SilindriteArv} {(SilindriteArv == ? "silinder" : "silindrit")}"; // teeb stringiks


    }
